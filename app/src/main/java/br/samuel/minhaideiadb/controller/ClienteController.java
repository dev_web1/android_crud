package br.samuel.minhaideiadb.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.samuel.minhaideiadb.api.AppUtil;
import br.samuel.minhaideiadb.datamodel.ClienteDataModel;
import br.samuel.minhaideiadb.datasource.AppDataBase;
import br.samuel.minhaideiadb.model.Cliente;

public class ClienteController extends AppDataBase implements iCrud<Cliente>{
  //  private AppDataBase conexao;
  //  private SQLiteDatabase banco;
    ContentValues dadoDoObjeto;

    public ClienteController(Context context) {
        super(context);
    //    conexao = new AppDataBase(context);
     //   banco = conexao.getWritableDatabase();
        Log.i(AppUtil.TAG, "ClienteControle: Conectado");
    }

    @Override
    public boolean incluir(Cliente obj) {
        dadoDoObjeto = new ContentValues();
        /* key - valor*/
        dadoDoObjeto.put(ClienteDataModel.NOME, obj.getNome());
        dadoDoObjeto.put(ClienteDataModel.EMAIL, obj.getEmail());
        /* enviar os dados -> dadoDoOjeto para a classe AppDataBase
        * utilizado um método capaz de fazer a inclusão do
        * objeto na base de dados*/
        return insert(ClienteDataModel.TABELA, dadoDoObjeto);
    }

    @Override
    public boolean deletar(int id) {
        return deleteOfId(ClienteDataModel.TABELA, id);
    }

    @Override
    public boolean alterar(Cliente obj) {
        dadoDoObjeto = new ContentValues();
        /* key - valor*/
        dadoDoObjeto.put(ClienteDataModel.ID, obj.getId());
        dadoDoObjeto.put(ClienteDataModel.NOME, obj.getNome());
        dadoDoObjeto.put(ClienteDataModel.EMAIL, obj.getEmail());
        /* enviar os dados -> dadoDoOjeto para a classe AppDataBase
         * utilizado um método capaz alterar o objeto na base de dados,
         * respeitando a condição registro = id(primary key)*/
        return alterarDados(ClienteDataModel.TABELA, dadoDoObjeto);
    }

    @Override
    public List<Cliente> listar() {
        return getAllClientes(ClienteDataModel.TABELA);
        }

    }

