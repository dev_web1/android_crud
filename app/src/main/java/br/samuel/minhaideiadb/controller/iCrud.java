package br.samuel.minhaideiadb.controller;

import java.util.List;

/* CRUD - Create, Retrieve, Updatae, Delete */
public interface iCrud<T> {
    /* Métodos para persistência de dados */
    //Incluir
    public boolean incluir(T obj);

    //Deletar
    public boolean deletar(int id);

    //Alterar
    public boolean alterar(T obj);

    //Listar
    public List<T> listar();
}