package br.samuel.minhaideiadb.controller;
import android.content.ContentValues;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;


import br.samuel.minhaideiadb.datamodel.ProdutoDataModule;
import br.samuel.minhaideiadb.datasource.AppDataBase;
import br.samuel.minhaideiadb.model.Produto;


public class ProdutoController extends AppDataBase implements iCrud<Produto> {

    ContentValues dadoDoObjeto;

    public ProdutoController(Context context) {
        super(context);
    }

    @Override
    public boolean incluir(Produto obj) {
        dadoDoObjeto = new ContentValues();
        dadoDoObjeto.put(ProdutoDataModule.NOME, obj.getNome());
        dadoDoObjeto.put(ProdutoDataModule.FORNECEDOR, obj.getFornecedor());
        return insert(ProdutoDataModule.TABELA, dadoDoObjeto);
    }

    @Override
    public boolean deletar(int id) {
        dadoDoObjeto = new ContentValues();
        dadoDoObjeto.put(ProdutoDataModule.ID, id);
        return false;
    }

    @Override
    public boolean alterar(Produto obj) {
        dadoDoObjeto = new ContentValues();
        dadoDoObjeto.put(ProdutoDataModule.ID, obj.getId());
        dadoDoObjeto.put(ProdutoDataModule.NOME, obj.getNome());
        dadoDoObjeto.put(ProdutoDataModule.FORNECEDOR, obj.getFornecedor());
        return false;
    }

    @Override
    public List<Produto> listar() {
        List<Produto> produtos = new ArrayList<>();
        return produtos;
    }
}
