package br.samuel.minhaideiadb.datamodel;

public class ProdutoDataModule {
    public static final String TABELA = "produto";
    public static final String ID = "id";
    public static final String NOME = "nome";
    public static final String FORNECEDOR = "fornecedor";
    public static String queryCriarTabela;

    /* - Script para criar a tabela produto */
    public static String criarTabela(){
        /* concatenação de strings */
        queryCriarTabela = "";
        queryCriarTabela += "CREATE TABLE IF NOT EXISTS "+TABELA+"(";
        queryCriarTabela += ID+" INTEGER PRIMARY KEY AUTOINCREMENT, ";
        queryCriarTabela += NOME+" TEXT, ";
        queryCriarTabela += FORNECEDOR+" TEXT";
        queryCriarTabela += ")";
        return queryCriarTabela;
    }
}