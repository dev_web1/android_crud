package br.samuel.minhaideiadb.datamodel;

/* -->> classe modelo objeto relacional
*  Toda classe DataModel possui a seguinte estrutura:
*  - 1. Atributo nome da tabela
*  - 2. Atributos um para um com o nome dos campos
*  - 3. Query para criar a tabela na base de dados e consultas gerais
*  - 4. Metodo para gerar o script para criar a tabela */
public class ClienteDataModel {
    public static final String TABELA = "cliente";
    public static final String ID = "id";
    public static final String NOME = "nome";
    public static final String EMAIL =  "email";
    private static String queryCriarTabela;


   /* - Script para criar a tabela cliente */
    public static String criarTabela(){
        /* concatenação de strings */
        queryCriarTabela = "";
        queryCriarTabela += "CREATE TABLE IF NOT EXISTS "+TABELA+"(";
        queryCriarTabela += ID+" INTEGER PRIMARY KEY AUTOINCREMENT, ";
        queryCriarTabela += NOME+" TEXT, ";
        queryCriarTabela += EMAIL+" TEXT";
        queryCriarTabela += ")";
        return queryCriarTabela;
    }
}
