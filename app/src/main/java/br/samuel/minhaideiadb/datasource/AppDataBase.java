package br.samuel.minhaideiadb.datasource;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import br.samuel.minhaideiadb.api.AppUtil;
import br.samuel.minhaideiadb.datamodel.ClienteDataModel;
import br.samuel.minhaideiadb.datamodel.ProdutoDataModule;
import br.samuel.minhaideiadb.model.Cliente;

/* classe para criar a base de dados*/
public class AppDataBase extends SQLiteOpenHelper {
    public static final String DB_NAME = "appideia.db";
    public static final int DB_VERSION  = 1;


    /* cria o objeto database */
   SQLiteDatabase database;

    public AppDataBase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);

        /* método de leitura e escrita na base de dados */
        database = getWritableDatabase();

        Log.i(AppUtil.TAG, "AppDataBase: Criando banco de dados..");
        Log.i(AppUtil.TAG, "AppDataBase onCreate: Criando Tabela cliente -> "+
                ClienteDataModel.criarTabela());
        Log.i(AppUtil.TAG, "AppDataBase onCreate: Criando Tabela produto -> "+
                ProdutoDataModule.criarTabela());
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(AppUtil.TAG, "AppDataBase onCreate: Criando Tabela cliente -> "+
                ClienteDataModel.criarTabela());
        Log.i(AppUtil.TAG, "AppDataBase onCreate: Criando Tabela produto -> "+
                ProdutoDataModule.criarTabela());
      /*executa o script para criar a tabela cliente */
        db.execSQL(ClienteDataModel.criarTabela());
       /*    db.execSQL("CREATE TABLE IF NOT EXISTS cliente(id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "nome varchar(100), email varchar(100)) ");*/
        /*executa o script para criar a tabela produto */
        db.execSQL(ProdutoDataModule.criarTabela());

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS cliente");
        db.execSQL("DROP TABLE IF EXISTS produto");
        onCreate(db);
        Log.i(AppUtil.TAG, "AppDataBase onUpgrade: Criando Tabela cliente -> "+
                ClienteDataModel.criarTabela());
    }
    public boolean insert(String tabela, ContentValues dados){
        /* caso estiver fechado a base ela é aberta */
        database = getWritableDatabase();
        boolean retorno = false;
        /*regra de negocio*/
        try {
            /* obs: o insert retorna o id da inserção  */
            retorno = database.insert(tabela, null, dados) > 0;
        }
        catch (Exception e){
            Log.i(AppUtil.TAG, "AppDataBase -> insert: "+ e.getMessage());
        }
        return retorno;
    }
    public boolean deleteOfId(String tabela, int id){
        /* caso estiver fechado a base ela é aberta */
        database = getWritableDatabase();
        boolean retorno = false;
        /*regra de negocio*/
        try {
            retorno = database.delete(tabela,"id = ?",
                    new String[]{String.valueOf(id)}) > 0;
        }
        catch (Exception e){
            Log.i(AppUtil.TAG, "AppDataBase -> delete: "+ e.getMessage());
        }
        return retorno;

    }
    public boolean alterarDados(String tabela, ContentValues dados){
        /* caso estiver fechado a base ela é aberta */
        database = getWritableDatabase();
        boolean retorno = false;
        /*regra de negocio*/
        try {
            retorno = database.update(tabela, dados, "id = ?",
                    new String[]{ String.valueOf(dados.get("id"))}) > 0;
        }
        catch (Exception e){
            Log.i(AppUtil.TAG, "AppDataBase -> update: "+ e.getMessage());
        }
        return retorno;
    }

    public List<Cliente> getAllClientes(String tabela){
        Cliente objCliente;
        database = getReadableDatabase();

        List<Cliente> clientes = new ArrayList<>();
        String sql = "select * from "+ tabela;
        Cursor cursor;
        cursor = database.rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {
                objCliente = new Cliente();
                objCliente.setId(cursor.getInt(cursor.getColumnIndex(ClienteDataModel.ID)));
                objCliente.setNome(cursor.getString(cursor.getColumnIndex(ClienteDataModel.NOME)));
                objCliente.setEmail(cursor.getString(cursor.getColumnIndex(ClienteDataModel.EMAIL)));
                clientes.add(objCliente);
                Log.i(AppUtil.TAG, "Cód.: "+objCliente.getId()+
                        " - Nome: "+objCliente.getNome()+
                        " - Email :"+objCliente.getEmail());
            } while (cursor.moveToNext());
        }
        return clientes;
    }
}
