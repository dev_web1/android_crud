package br.samuel.minhaideiadb.model;

import android.util.Log;

import java.io.Serializable;

import br.samuel.minhaideiadb.api.AppUtil;
import br.samuel.minhaideiadb.controller.iCrud;
//INTERESSANTE IMPLEMENTAR JUNTO A CLASSE SERIA..para enviar para outras telas
public class Cliente implements Serializable {
    private int id;
    private String nome;
    private String email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
