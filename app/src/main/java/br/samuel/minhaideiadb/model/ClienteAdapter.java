package br.samuel.minhaideiadb.model;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.samuel.minhaideiadb.R;
/*  CLASSE ClienteAdapter - 27.01.2022 - para inflar o layout item.xml com
* dados do Cliente: nome, cpf, email */

public class ClienteAdapter extends BaseAdapter {
    private Activity activity;
    private List<Cliente> listaClientes;

    public ClienteAdapter(Activity activity, List<Cliente> listaClientes) {
        this.activity = activity;
        this.listaClientes = listaClientes;
    }
    @Override
    public int getCount() {
      return listaClientes.size();
    }

    @Override
    public Object getItem(int i) {
            return listaClientes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return listaClientes.get(i).getId();
    }

    /* método para inflar o layout clientes_itens.xml
    com os dados da base de dadps*/
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = activity.getLayoutInflater().inflate(R.layout.clientes_itens, viewGroup, false);

        /* da view passa os dados para os campos no clientes_itens.xml */
        TextView txtNome = v.findViewById(R.id.txtNome);
        TextView txtId = v.findViewById(R.id.txtId);
        TextView txtEmail = v.findViewById(R.id.txtEmail);

        /*-- instancia o objeto e encapsula os dadso --*/
        Cliente a = listaClientes.get(i);
        txtNome.setText(a.getNome());
        /*-- convert tipo int para tipo string
        * para atribuir ao componente visual TextView --*/
        txtId.setText(Integer.toString(a.getId()));
        txtEmail.setText(a.getEmail());
        return v;
    }
}
