package br.samuel.minhaideiadb.view;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewDebug;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.samuel.minhaideiadb.R;
import br.samuel.minhaideiadb.model.Cliente;
import br.samuel.minhaideiadb.model.ClienteAdapter;
import br.samuel.minhaideiadb.controller.ClienteController;

public class Listar_Clientes_Activity extends AppCompatActivity {
    private ListView lwLista;
    private ClienteController clienteController;
    private List<Cliente> clienteLista;
    private final List<Cliente> clienteFiltrados = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientes);

        lwLista = findViewById(R.id.lwListaClientes);
        clienteController = new ClienteController(getApplicationContext());
        /* atribui na lista cliente todos os dados do banco */
        clienteLista = clienteController.listar();
        /* atribui a lista ao vetor de listas */
        clienteFiltrados.addAll(clienteLista);

        /*UTILIZAMOS O ADAPTADOR DO ANDROID PARA INSERIR A LISTA NO COMPONENTE VISUAL LISTVIEW
        ArrayAdapter<Cliente> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, alunosFiltrados);
        lista.setAdapter(adapter);
        ---Substituido pelo processo abaixo na aula 7 para mostrar todos os dados do aluno */
        ClienteAdapter adapter = new  ClienteAdapter(this, clienteFiltrados);
        lwLista.setAdapter(adapter);

        /* AULA 5 - CRIANDO MENU DE CONTEXTO COM DOIS ITENS: EXCLUIR E ATUALIZAR
           QUANDO O USUARIO CLICAR NA LISTA ACIONA UM MÉTODO DA ACTIVITY QUE CHAMA O MENU DE CONTEXTO
         */
        registerForContextMenu(lwLista);
    }

    /*----- AULA 3 - CRUD CRIAÇÃO DO MENU NA TELA listar_alunos.xml
     * ---evento para onclick no item search  do menu */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater i = getMenuInflater();
        i.inflate(R.menu.menu_principal, menu);

        //MÉTODO VERIFICA O ITEM SEARCH DO MENU FOI CLICADO
        //CAPTURA O QUE FOI DIGITADO NO CAMPO SEARCH
        SearchView sv = (SearchView) menu.findItem(R.id.app_bar_search).getActionView();
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                //System.out.println("Você digitou "+ s);
                pesquisarCliente(s);
                return false;
            }
        });
        // return super.onCreateOptionsMenu(menu); */
        return true;
    }

    /*----- AULA 3 - CRUD CRIAÇÃO DO MENU NA TELA listar_alunos.xml
     * ---resposta ao evento para onclick no item search  do menu */
    public void pesquisarCliente(String nome){
        clienteFiltrados.clear();
        for(Cliente a : clienteLista){
            if(a.getNome().toLowerCase().contains(nome.toLowerCase())){
                clienteFiltrados.add(a);
                // System.out.println("Você digitou "+ a.getNome().toString());
            }
        }//fecha o  laço for
        lwLista.invalidateViews();
    }

    /* AULA 5 - CRIANDO MENU DE CONTEXTO COM DOIS ITENS: EXCLUIR E ATUALIZAR
          QUANDO O USUARIO CLICAR NA LISTA      */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_contexto, menu);
    }

    /* AULA 5 - CRIANDO OS MÉTODOS PARA EXCLUIR E ATUALIZAR
    PARA O MENU DE CONTEXTO DA LISTA*/
    public void excluir(MenuItem item){
        AdapterView.AdapterContextMenuInfo menuInfo =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        /*  RETORNA O ID ALUNO */
        final Cliente  idCliente = clienteFiltrados.get(menuInfo.position);
        int id = clienteFiltrados.get(menuInfo.position).getId();

        /*--  mostra mensagens para o usuário --*/
        Log.i("ver", "ID do cliente: "+id);
        Toast.makeText(this, "Id :"+id , Toast.LENGTH_LONG).show();

        /* CRIANDO UMA MSG DE CONFIRMAÇÃO PARA EXCLUSÃO*/
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Atenção")
                .setMessage("Tem certeza que deseja excluir o registro da base de dados?")
                .setNegativeButton("Não", null)
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    /* metodo de excluir o registro do ListView e da base de dados*/
                    public void onClick(DialogInterface dialogInterface, int i) {
                        clienteFiltrados.remove(idCliente);
                        /* remove da lista */
                        clienteLista.remove(idCliente);
                        /* remove da base de dados */
                        clienteController.deletar(id);
                        lwLista.invalidateViews();
                    }
                }).create();//fechamento do click no sim
        dialog.show();

    }
    public void atualizar(MenuItem item){
        AdapterView.AdapterContextMenuInfo menuInfo =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        /*  RETORNA O ID ALUNO SELECIONADO */
        final Cliente  idCliente = clienteFiltrados.get(menuInfo.position);
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("cliente", idCliente);
        startActivity(intent);

    }

    /*      RETORNA A TELA DE CADASTRO */
    public void telaPrincipal(MenuItem item){
        Intent it = new Intent(this, MainActivity.class);
        startActivity(it);
    }
}