package br.samuel.minhaideiadb.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import br.samuel.minhaideiadb.R;
import br.samuel.minhaideiadb.api.AppUtil;
import br.samuel.minhaideiadb.controller.ClienteController;
import br.samuel.minhaideiadb.model.Cliente;

public class MainActivity extends AppCompatActivity {
    private EditText edtNome, edtEmail;
    private Cliente cliente = null;
    ClienteController clienteController;
    Button btnSalvar;
    //Cliente cliente;
    private boolean dadosOk;

    {
        dadosOk = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNome = findViewById(R.id.edtNome);
        edtEmail = findViewById(R.id.edtEmail);
        btnSalvar = findViewById(R.id.btnSalvar);

        clienteController = new ClienteController(getApplicationContext());

        /* AULA 6 - ATUALIZAR ALUNO - capturando o id aluno enviado da tela
         * listar_aluno */
        Intent intent = getIntent();
        /* se vier algo pela intent entra no if para coleta dos dados */
        if (intent.hasExtra("cliente")){
            /* atribui a cliente  o que vem da intent */
            cliente = (Cliente) intent.getSerializableExtra("cliente");
            /* atribui os valores recebidos pela intent aos campos do
             * formulário */
            edtNome.setText(cliente.getNome());
            edtEmail.setText(cliente.getEmail());

        }
        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if (validarCampos()){
                    salvar();
                }
            }
        });


        Log.i(AppUtil.TAG, "onCreate: AppMinhaIdeaBd");



        //excluir();
        // alterar();
        listar();

    }
    public boolean validarCampos(){
        dadosOk = false;
        if ((TextUtils.isEmpty(edtNome.getText().toString())) ||  (TextUtils.isEmpty(edtEmail.getText().toString()))) {
            Toast.makeText(this, "Preencha todos os campos...obrigado.", Toast.LENGTH_LONG).show();
            dadosOk = false;
        }else {
            dadosOk=true;
        }
        return dadosOk;
    }
    public void salvar() {
        /*CRIANDO O OBJETO cliente e instanciando a CLASSE Cliente se
         * a variavel cliente esteja vazia, caso contrario é atualizada na classe */
        /* validano os campos */
            if (cliente == null) {
                cliente = new Cliente();
                cliente.setNome(edtNome.getText().toString());
                cliente.setEmail(edtEmail.getText().toString());

                /* --  método que liga com o insert do sql -- */
                if (clienteController.incluir(cliente)) {
                    Toast.makeText(this, "O registro foi inserido com sucesso.", Toast.LENGTH_LONG).show();
                    Log.i("res", "Nome:" + edtNome.getText() + " - email:" + edtEmail.getText());
                } else {
                    Toast.makeText(this, "O registro não foi inserido na base de dados", Toast.LENGTH_LONG).show();
                    Log.i("res", "O registro não foi inserido na base de dados");
                }
            } else {
                cliente.setNome(edtNome.getText().toString());
                cliente.setEmail(edtEmail.getText().toString());
                if (clienteController.alterar(cliente)) {
                    Toast.makeText(this, "O registro foi atualizado com sucesso.", Toast.LENGTH_LONG).show();
                    Log.i("res", " Registro atualizado -> Nome:" + edtNome.getText() + " - email:" + edtEmail.getText());
                } else {
                    Toast.makeText(this, "O registro não foi alterado na base de dados", Toast.LENGTH_LONG).show();
                    Log.i("res", "O registro não foi alterado na base de dados");
                }
            }
    }


    public void incluir(){
        if (clienteController.incluir(cliente)){
            Toast.makeText(this, "Inclusão executada com sucesso"+cliente.getNome(), Toast.LENGTH_SHORT).show();
            Log.i(AppUtil.TAG, "MainAcitivity: inclusão ok."+cliente.getNome());
        }
        else{
            Toast.makeText(this, "Erro na Inclusão de "+cliente.getNome(), Toast.LENGTH_SHORT).show();
            Log.i(AppUtil.TAG, "MainAcitivity: Erro na Inclusão de "+cliente.getNome());
        }
    }
    public void excluir(){
        if (clienteController.deletar(cliente.getId())){
            Toast.makeText(this, "Exclusão executada com sucesso"+cliente.getId(), Toast.LENGTH_SHORT).show();
            Log.i(AppUtil.TAG, "MainAcitivity: Exclusão ok."+cliente.getId());
        }
        else{
            Toast.makeText(this, "Erro na Exclusão de "+cliente.getId(), Toast.LENGTH_SHORT).show();
            Log.i(AppUtil.TAG, "MainAcitivity: Erro na Exclusão de "+cliente.getId());
        }
    }

    public void alterar(){
        if(clienteController.alterar(cliente)){
            Toast.makeText(this, "Os dados foram alterados com sucesso"+cliente.getNome(), Toast.LENGTH_SHORT).show();
            Log.i(AppUtil.TAG, "MainAcitivity: Update ok."+cliente.getNome());
        }
        else{
            Toast.makeText(this, "Erro na Exclusão de "+cliente.getNome(), Toast.LENGTH_SHORT).show();
            Log.i(AppUtil.TAG, "MainAcitivity: Erro na Update de "+cliente.getNome());
        }
    }
    /* exemplo de lista ou arrays ou vetores */
        public void listar(){
            for (Cliente obj:clienteController.listar()) {
                Log.i(AppUtil.TAG, "Main-> Lista: "+obj.getId()+ ". Nome: "+ obj.getNome()+
                        " - Email: "+obj.getEmail());
            }
        }
        public void telaListarClientes(View view){
            Intent it = new Intent(this, Listar_Clientes_Activity.class);
            startActivity(it);
        }
}